// Generated by CoffeeScript 1.7.1
(function() {
  var cluster, config, exec, fs, http, path, root;

  cluster = require("cluster");

  http = require('http');

  fs = require("fs");

  config = require("../config");

  exec = require('child_process').exec;

  path = require('path');

  root = path.dirname(process.mainModule.filename);

  process.on("message", function(list) {
    return list.forEach(function(item) {
      var r, url;
      url = config.host + config.remotePath + item.pathDatePrefix + item.filename;
      console.log(url);
      r = http.get(url, function(res) {
        var imagedata;
        console.log(item);
        imagedata = '';
        res.setEncoding('binary');
        return res.on('error', function(err) {
          return console.log("Http error:", err);
        }).on('data', function(chunk) {
          return imagedata += chunk;
        }).on('end', function() {
          console.log("download ended");
          return fs.mkdir(root + '/images/' + item.pathDatePrefix, function() {
            return fs.exists(root + '/images/' + item.pathDatePrefix + item.filename, function(exists) {
              if (exists) {
                fs.unlinkSync('images/' + item.pathDatePrefix + item.filename);
                console.log("image deleted");
              }
              return fs.writeFile(root + '/images/' + item.pathDatePrefix + item.filename, imagedata, 'binary', function(err) {
                var command, filename;
                if (err) {
                  throw err;
                }
                console.log("image writed");
                filename = root + '/images/' + item.pathDatePrefix + item.filename.replace(/\//g, path.sep);
                command = config.print.app + ' ' + filename + ' ' + config.print.opts;
                console.log(command);
                return exec(command, function(err, stdout, stderr) {
                  if (err) {
                    throw err;
                  }
                  console.log('File printed');
                  return process.send(item._id, item.pool_id);
                });
              });
            });
          });
        });
      });
      return r.on('error', function(err) {
        return console.log("Http error:", err);
      });
    });
  });

}).call(this);
