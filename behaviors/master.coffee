socketIO = require 'socket.io-client'
config = require '../config'
io = socketIO.connect config.host, config.connectOpts
cluster = require("cluster")

pool = {}

cluster.on "exit", (worker, code, signal) ->
  console.log "worker is dead", code, signal

io.on "connect", (socket) ->
  console.log "connect"

#tell socket.io to never give up :)
io.on 'error', ->
  io.socket.reconnect()

io.on "reconnecting", (delay, attempt) ->
  console.log "Socket reconnecting:", attempt, "; Next reconnect:", delay / 1000, "sec"

io.on "unprintedList", (data) ->
  toWork = []
  for item in data
    unless pool[item._id]?
      pool[item._id] = false
      toWork.push item
    else
      console.log pool
      if pool[item._id]
        console.log "Send file printed cuz of double", item._id
        io.emit 'filePrinted', item._id
      console.log "Dublicated", item.filename


  if(toWork.length > 0)
    worker = cluster.fork()

    worker
    .on "message", (id, pid)->
        console.log id
        pool[id] = true
        console.log "Send file printed"
        io.emit 'filePrinted', id
    .send toWork


