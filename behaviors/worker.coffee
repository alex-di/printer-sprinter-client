cluster = require "cluster"
http = require 'http'
fs = require "fs"
config = require "../config"
exec = require('child_process').exec
path = require('path')

root = path.dirname(process.mainModule.filename)

process.on "message", (list) ->

  # Fake delay for correct socket work
  #setTimeout(function () {
  list.forEach (item) ->
    url = config.host + config.remotePath + item.pathDatePrefix + item.filename
    console.log url

    r = http.get url, (res) ->
      console.log item

      imagedata = ''
      res
      .setEncoding('binary')

      res

      .on 'error', (err) ->
        console.log("Http error:", err)

      .on 'data', (chunk) ->
        imagedata += chunk


      .on 'end', ->
        console.log "download ended"
        fs.mkdir root + '/images/' + item.pathDatePrefix, ->
          fs.exists root + '/images/' + item.pathDatePrefix + item.filename, (exists) ->
            if exists
              fs.unlinkSync('images/' + item.pathDatePrefix + item.filename)
              console.log "image deleted"
            fs.writeFile root + '/images/' + item.pathDatePrefix + item.filename, imagedata, 'binary', (err) ->

              throw err if err

              console.log "image writed"

              filename = root + '/images/' + item.pathDatePrefix + item.filename
                .replace /\//g, path.sep

              command = config.print.app + ' ' + filename + ' ' + config.print.opts;
              console.log command

              exec command, (err, stdout, stderr) ->
                throw err if err
                console.log 'File printed'

                process.send item._id, item.pool_id

    r.on 'error', (err) ->
        console.log("Http error:", err)


  #  }, 1000)

