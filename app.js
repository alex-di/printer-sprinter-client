var cluster = require("cluster")

//process.on('uncaughtException', function(err){
//    console.log("Uncaught exception:" + err)
//})
if (cluster.isMaster) {
    require("./behaviors/master");
} else {
    require("./behaviors/worker")
}